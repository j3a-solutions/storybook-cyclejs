# Storybook for Cycle.js

Storybook for Cycle.js is a UI development environment for your Cycle.js components.
With it, you can visualize different states of your UI components and develop them interactively.

Storybook runs outside of your app.
So you can develop UI components in isolation without worrying about app specific dependencies and requirements.

## Getting Started

```sh
npm i -g @storybook/cli
cd my-cyclejs-app
getstorybook
```

For more information visit: [storybook.js.org](https://storybook.js.org)

* * *

Storybook also comes with a lot of [addons](https://storybook.js.org/addons/introduction) and a great API to customize as you wish.
You can also build a [static version](https://storybook.js.org/basics/exporting-storybook) of your storybook and deploy it anywhere you want.

## Typescript

This package is written in typescript and ships with type definitions.

## Demo

This package includes a small demo used for development purposes. To try it out just install all dev dependencies and run `yarn start`.

## Docs

-   [Basics](https://storybook.js.org/basics/introduction)
-   [Configurations](https://storybook.js.org/configurations/default-config)
-   [Addons](https://storybook.js.org/addons/introduction)
