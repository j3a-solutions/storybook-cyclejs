import { run } from '@cycle/run';
import {makeDOMDriver } from '@cycle/dom';
import { stripIndents } from 'common-tags';
// import isReactRenderable from './element_check';

const drivers = {
  DOM: makeDOMDriver('#root')
};

export default function renderMain({
  story,
  selectedKind,
  selectedStory,
  showMain,
  showError
}: any) {

  const main = story(drivers);
  if (!main) {
    showError({
      title: `Expecting your element function to return sinks for the story: "${selectedStory}" of "${selectedKind}".`,
      description: stripIndents`
        Did you forget to return sinks from the story?
      `,
    });
    return;
  }

  showMain();
  run(main, drivers);
}