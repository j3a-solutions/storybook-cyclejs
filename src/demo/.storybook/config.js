import { configure } from "../../client/preview";

function loadStories() {
  require('../index');
}
configure(loadStories, module);
