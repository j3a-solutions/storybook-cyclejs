import xs from 'xstream';
import {div, button, p, DOMSource} from '@cycle/dom';

interface Sources {
  DOM: DOMSource
}

export default function demo(sources: Sources) {
  const action$ = xs.merge(
    sources.DOM.select('.decrement').events('click').map(() => -1),
    sources.DOM.select('.increment').events('click').map(() => +1)
  );
  const count$ = action$.fold((acc, x) => acc + x, 0);
  const vdom$ = count$.map(count =>
    div([
      button('.decrement', 'Decrements'),
      button('.increment', 'Increment'),
      p('Counter: ' + count)
    ])
  );
  return {
    DOM: vdom$,
  };
}