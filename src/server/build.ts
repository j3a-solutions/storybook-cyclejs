import { buildStatic } from '@storybook/core/server';
import * as options from './options';

buildStatic(options);