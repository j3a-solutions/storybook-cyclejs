import { buildDev } from '@storybook/core/server';

import * as options from './options';

buildDev(options);