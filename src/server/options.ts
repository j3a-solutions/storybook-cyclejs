const packageJson = require('../../package.json');

import wrapInitialConfig from './wrapInitialConfig';

module.exports = {
  packageJson,
  wrapInitialConfig,
};