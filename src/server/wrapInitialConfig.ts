import { Configuration } from 'webpack'

const wrappedConfig = (config: Configuration): Configuration => config;

export default wrappedConfig;